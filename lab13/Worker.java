package Models;

import Utilities.GeneralTaxingConstants;

import java.util.Objects;
import java.util.UUID;

public class Worker extends Person{
    protected int childernNumber;
    protected int salaryInput;
    protected boolean socialClass;

    private final double salaryInputClassHigh = 0.3;
    private final double salaryInputClassMid= 0.2;
    private final double salaryInputClassLow = 0.1;

    private final int salaryInputHighThreshold = 10000;
    private final int salaryInputLowThreshold = 7500;

    public Worker(UUID uuid,String firstName, String lastName, double accountBalance, int childernNumber, int salaryInput, boolean socialClass){
        super(uuid,firstName,lastName,accountBalance);
        this.childernNumber=childernNumber;
        this.salaryInput = salaryInput;
        this.socialClass=socialClass;
    }

    public Worker(Worker original){
        super(UUID.randomUUID(),original.firstName,original.lastname,original.accountBalance);
        this.childernNumber= original.childernNumber;
        this.salaryInput =original.salaryInput;
        this.socialClass=original.socialClass;
    }

    public int getSalaryInput() {
        return salaryInput;
    }

    public void setSalaryInput(int salaryInput) {
        this.salaryInput = salaryInput;
    }

    public int getChildernNumber() {
        return childernNumber;
    }

    public void setChildernNumber(int childernNumber) {
        this.childernNumber = childernNumber;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "uuid=" + uuid +
                ", firstName='" + firstName + '\'' +
                ", lastname='" + lastname + '\'' +
                ", accountBalance=" + accountBalance +
                ", childernNumber=" + childernNumber +
                ", salaryInput=" + salaryInput +
                ", socialClass=" + socialClass +
                ", salaryInputClassHigh=" + salaryInputClassHigh +
                ", salaryInputClassMid=" + salaryInputClassMid +
                ", salaryInputClassLow=" + salaryInputClassLow +
                ", salaryInputHighThreshold=" + salaryInputHighThreshold +
                ", salaryInputLowThreshold=" + salaryInputLowThreshold +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Worker worker = (Worker) o;
        return childernNumber == worker.childernNumber && salaryInput == worker.salaryInput && socialClass == worker.socialClass && Double.compare(worker.salaryInputClassHigh, salaryInputClassHigh) == 0 && Double.compare(worker.salaryInputClassMid, salaryInputClassMid) == 0 && Double.compare(worker.salaryInputClassLow, salaryInputClassLow) == 0 && salaryInputHighThreshold == worker.salaryInputHighThreshold && salaryInputLowThreshold == worker.salaryInputLowThreshold;
    }

    @Override
    public int hashCode() {
        return Objects.hash(childernNumber, salaryInput, socialClass, salaryInputClassHigh, salaryInputClassMid, salaryInputClassLow, salaryInputHighThreshold, salaryInputLowThreshold);
    }

    @Override
    public boolean applyTax() {
        double calculatedTax;
        if(socialClass == true){
            return false;
        }
        else if (salaryInput>salaryInputHighThreshold){
            calculatedTax = accountBalance * salaryInputHighThreshold * GeneralTaxingConstants.TAX_WORKER;
            calculatedTax=calculatedTax-(calculatedTax* (childernNumber/10d));
            accountBalance=accountBalance-calculatedTax;
            return true;
        }else if ( salaryInput>salaryInputLowThreshold && salaryInput<salaryInputHighThreshold){
            calculatedTax = accountBalance * salaryInputHighThreshold*GeneralTaxingConstants.TAX_WORKER;
            calculatedTax = calculatedTax-(calculatedTax*(childernNumber/10d));
            accountBalance = accountBalance - calculatedTax;
            return true;
        }else {
            calculatedTax = accountBalance*salaryInputHighThreshold*GeneralTaxingConstants.TAX_WORKER;
            calculatedTax = calculatedTax-(calculatedTax*(childernNumber/10d));
            accountBalance = accountBalance - calculatedTax;
            return true;
        }

    }

    @Override
    public double getCalculatedTax() {
        double calculatedTax;
        if (salaryInput>salaryInputHighThreshold){
            calculatedTax = accountBalance * salaryInputHighThreshold * GeneralTaxingConstants.TAX_WORKER;
            calculatedTax=calculatedTax-(calculatedTax* (childernNumber/10d));

            return calculatedTax;
        }else if ( salaryInput>salaryInputLowThreshold && salaryInput<salaryInputHighThreshold){
            calculatedTax = accountBalance * salaryInputHighThreshold*GeneralTaxingConstants.TAX_WORKER;
            calculatedTax = calculatedTax-(calculatedTax*(childernNumber/10d));

            return calculatedTax;
        }else {
            calculatedTax = accountBalance*salaryInputHighThreshold*GeneralTaxingConstants.TAX_WORKER;
            calculatedTax = calculatedTax-(calculatedTax*(childernNumber/10d));

            return calculatedTax;
        }
    }
}
