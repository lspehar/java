package com.example.lab12;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class InsideView {
    @FXML
    private Label lblUser;
    @FXML
    private Button btnLogout;

    private Parent root;
    private Stage stage;
    private Scene scene;

    @FXML
    void setUser(String user) {
        lblUser.setText(user);
    }

    // button za return na login screen
    @FXML
    void onLoginClicked(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login-view.fxml"));
        root = loader.load();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root,600,400);
        stage.setScene(scene);
        stage.show();

    }

}
