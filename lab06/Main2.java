// Printable.java (interface):

@FunctionalInterface // allows samo 1 metodu

public interface Printable {
    void print();
}



// Macka.java:

public class Macka implements Printable {
    String ime;
    int god;

    Macka(String ime, int god) {
        this.ime = ime;
        this.god = god;
    }

    // getteri
    public String getIme() {
        return ime;
    }

    public int getGod() {
        return god;
    }

    // setteri
    public void setIme() {
        this.ime = ime;
    }

    public void setGod() {
        this.god = god;
    }

    // interface:
    public void print() {
        System.out.println("Meow meow!\n");
    }

}



// Main.java:

public class Main2 {

    public static void printThing(Printable thing) {
        thing.print();
    }

    public static void main(String[] args) {
        Macka m1 = new Macka("Kitty", 4);
        m1.print();

        // metoda:
        printThing(m1);

        // lambda:
        printThing(() -> System.out.println("Meow!\n"));

    }
}
