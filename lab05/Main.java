// Vozilo.java
public class Vozilo {
    private String registracijskaOznaka;
    private int brojSjedala;
    private int brojVrata;

    public Vozilo(String registracijskaOznaka, int brojSjedala, int brojVrata) {
        this.registracijskaOznaka = registracijskaOznaka;
        this.brojSjedala = brojSjedala;
        this.brojVrata = brojVrata;
    }

    // get
    public String getRega() {
        return registracijskaOznaka;
    }

    public int getSjediste() {
        return brojSjedala;
    }

    public int getVrata() {
        return brojVrata;
    }

    // set
    public void setRega(String registracijskaOznaka) {
        this.registracijskaOznaka = registracijskaOznaka;
    }

    public void setSjediste(int brojSjedala) {
        this.brojSjedala = brojSjedala;
    }

    public void setVrata(int brojVrata) {
        this.brojVrata = brojVrata;
    }
}



// Auto.java
public class Auto extends Vozilo implements iVaranje {
    protected String marka;
    protected double rezervar;

    public Auto(String marka, double rezervar) {
        super("BJ-144-DH", 5, 3);

        this.marka = marka;
        this.rezervar = rezervar;
    }

    // get
    public String getMarka() {
        return marka;
    }

    public double getRezervar() {
        return rezervar;
    }

    // set
    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setRezervar(double rezervar) {
        this.rezervar = rezervar;
    }

    // metoda:
    public void vozi() {
        System.out.println("Auto se vozi!");
    }

    // interface - letenje:
    public void letenje() {
        System.out.println(getMarka() + " (" + getRega() + ") leti po autoputu!");
    }

    // interface - kretanjePoVodi:
    public void kretanjePoVodi() {
        System.out.println(getMarka() + " (" + getRega() + ") se krece po vodi.");
    }
}



// Osoba.java
public class Osoba implements iVaranje {
    private String ime;
    private String prezime;
    private int oib;
    private int starost;

    public Osoba(String ime, String prezime, int oib, int starost) {
        this.ime = ime;
        this.prezime = prezime;
        this.oib = oib;
        this.starost = starost;
    }

    // get
    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public int getOIB() {
        return oib;
    }

    public int getStarost() {
        return starost;
    }

    // set
    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setOIB(int oib) {
        this.oib = oib;
    }

    public void setStarost(int starost) {
        this.starost = starost;
    }

    // metode:
    public void predstavljanje() {
        System.out.println("Zdravo! Ja sam " + getIme() + " " + getPrezime() + ". OIB mi je " + getOIB() + " i imam " + getStarost() + " godina.\n");
    }

    public void hodanje() {
        System.out.println(getIme() + " " + getPrezime() + " trenutno hoda.\n");
    }

    // interface:
    public void letenje() {
        System.out.println(getIme() + " " + getPrezime() + " je popio/la Red Bull i dobio/la krila!\n");
    }

    public void kretanjePoVodi() {
        System.out.println(getIme() + " " + getPrezime() + " glumi da je isus i hoda po vodi.\n");
    }
}



// iVaranje.java
public interface iVaranje {
    void letenje();
    void kretanjePoVodi();
}



// Main.java
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Auto GolfMK5 = new Auto("Volkswagen", 50);
        Osoba fahrer = new Osoba("Jochen", "Mass", 012345, 66);

        iVaranje [] array = new iVaranje[4];

        System.out.println("Unesi neki string: ");
        Scanner scn = new Scanner(System.in);
        String str = scn.nextLine();
        //System.out.println("Input: " + str);

        if (str.equals("seaways")) {
            GolfMK5.kretanjePoVodi();
            fahrer.kretanjePoVodi();
        }

        if (str.equals("redbull")) {
            GolfMK5.letenje();
            fahrer.letenje();
        }

        array[0] = GolfMK5;
        array[1] = fahrer;

    }
}
