package Models.Database;

import Models.Pensioner;
import Models.Worker;
import Models.WorkerRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class WorkerDatabase implements WorkerRepository {
    // db:
    List<Worker> database = new ArrayList<Worker>();

    // logger:
    static Logger log = Logger.getLogger("Models/Database/WorkerDatabase.java");

    // konstruktor:
    public WorkerDatabase() {
        FileHandler fileHandler;

        try {
            fileHandler = new FileHandler("WorkerDatabase.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            log.info("Logger initialized!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("DB init!");

        // inserti:
        database.add(new Worker(UUID.randomUUID(), "Jovanka", "Jovanovic", 99657, 2, 40, false));
        database.add(new Worker(UUID.randomUUID(), "Ivan", "Ivsic", 49670, 4, 15, true));
        database.add(new Worker(UUID.randomUUID(), "Vladislav", "Slavovic", 1089670, 1, 65, false));
        database.add(new Worker(UUID.randomUUID(), "Tanja", "Savic", 468950, 0, 97, false));
    }

    @Override
    public Worker get(UUID id) {
        for (Worker w : database) {
            if (w.getUuid().compareTo(id) == 0) {
                return w;
            }
        }

        return null;
    }

    @Override
    public void add(Worker worker) {
        database.add(worker);
    }

    @Override
    public boolean update(Worker worker) {
        for (Worker w : database) {
            if (w.getUuid() == worker.getUuid()) {
                w.setFirstName(worker.getFirstName());
                w.setLastname(worker.getLastname());
                w.setAccountBalance(worker.getAccountBalance());
                w.setChildernNumber(worker.getChildernNumber());
                w.setSalaryInput(worker.getSalaryInput());
                // social status?

                return true;
            }
        }
        database.add(worker);
        return false;
    }

    @Override
    public void remove(Worker worker) {
        database.remove(worker);
    }

    @Override
    public List<Worker> getAllWorkers() {
        log.warning("GetAllWorkers called!");

        return database;
    }
}

