// Smjerovi.java
public enum Smjerovi {
    SJEVER("S"),
    JUG("J"),
    ISTOK("I"),
    ZAPAD("Z"),
    SJEVEROISTOK("SI"),
    SJEVEROZAPAD("SZ"),
    JUGOISTOK("JI"),
    JUGOZAPAD("JZ");

    final String kratica;

    Smjerovi(String kratica) {
        this.kratica = kratica;
    }

}



// Kompas.java
public class Kompas {
    double igla;

    Kompas(double igla) {
        this.igla = igla;
    }

    // get:
    double getIgla() {
        return igla;
    }

    // set:
    void setIgla(double igla) {
        this.igla = igla;
    }

    // pokazi smjer:
    void pokaziSmjer() {
        if (igla == 0) {
            System.out.println(Smjerovi.SJEVER.kratica);
        } else if (igla == 90) {
            System.out.println(Smjerovi.ISTOK.kratica);
        } else if (igla == 180) {
            System.out.println(Smjerovi.JUG.kratica);
        } else if (igla == 270) {
            System.out.println(Smjerovi.ZAPAD.kratica);
        } else if (igla > 0 && igla < 90) {
            System.out.println(Smjerovi.SJEVEROISTOK.kratica);
        } else if (igla > 90 && igla < 180) {
            System.out.println(Smjerovi.JUGOISTOK.kratica);
        } else if (igla > 180 && igla < 270) {
            System.out.println(Smjerovi.JUGOZAPAD.kratica);
        } else if (igla > 270 && igla < 360) {
            System.out.println(Smjerovi.SJEVEROZAPAD.kratica);
        } else {
            System.out.println("Ne nalazite se na Zemlji.\n");
        }
    }

}



// Main.java
public class Main {
    public static void main(String[] args) {
        Kompas k1 = new Kompas(0);
        k1.pokaziSmjer();

        // nova igla:
        k1.setIgla(169.7);
        k1.pokaziSmjer();
    }
}
