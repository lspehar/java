// CPU.java
public class CPU {
    public double price;

    // unutarnja klasa - Processor (inner class):
    class Processor {
        public double cores;
        public String manufacturer;

        // geteri:
        public double getCores() {
            return 4;
        }

        public String getManufacturer() {
            return "Intel";
        }
    }

    // unutarnja klasa - RAM:
    protected class RAM {
        protected double memory;
        protected double clockSpeed;
        protected double cache;
        protected String manufacturer;

        // geteri za RAM:
        public double getMemory() {
            return 16.0;
        }

        public double getClock() {
            return 2133;
        }

        public double getCache() {
            return 256;
        }

        public String getManufacturer() {
            return "Kingston";
        }
    }
}



// Main2.java
public class Main2 {
    public static void main(String[] args) {
        CPU cpu1 = new CPU();
        CPU.Processor i5 = cpu1.new Processor();
        CPU.RAM savage = cpu1.new RAM();

        System.out.println("RAM Clock: " + savage.getClock() + " MHz");
        System.out.println("RAM Cache: " + savage.getCache() + " MB");

        /*
        // case 2 - static Processor i RAM:
        CPU.Processor i7 = new CPU.Processor(); // ako su static
        CPU.RAM kingston = new CPU.RAM(); // static

        System.out.println("RAM Clock: " + kingston.getClock() + " MHz");
        System.out.println("RAM Cache: " + kingston.getCache() + " MB");
         */
    }
}
