// Animal.java
public class Animal {
    public String ime;

    public Animal(String ime) {
        this.ime = ime;
    }

    public void makeNoise() {
        System.out.println("Whoof! I'm a dog!");
    }
}



// Main.java
public class Main {
    public static void main(String[] args) {
        // normalni objekt
        Animal dog = new Animal("Adi");
        dog.makeNoise();

        // napravi novi objekt + anonimna klasa:
        Animal snake = new Animal("Mamba") {
          public void makeNoise() {
              System.out.println("CsSsSs!");
          }
        };

        snake.makeNoise();
    }
}
