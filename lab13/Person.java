package Models;

import java.util.UUID;

public abstract class Person {

    protected UUID uuid;
    protected String firstName;
    protected String lastname;
    protected double accountBalance;

    Person(UUID uuid, String firstName, String lastname, double accountBalance){
        this.uuid=uuid;
        this.firstName=firstName;
        this.lastname = lastname;
        this.accountBalance = accountBalance;
    }

    public  abstract boolean applyTax();
    public  abstract double getCalculatedTax();

    public UUID getUuid() {return uuid;}

    public double getAccountBalance() {      return accountBalance;  }

    public String getFirstName() { return firstName;    }

    public String getLastname() { return lastname;  }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}

