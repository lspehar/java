// Auto.java
public class Auto {
    public static final double LITRE_PER_100KM = 6.5;
    double topSpeed;
    double fuelTank;
    int doors;

    public Auto() {
    }

    public Auto(double topSpeed, double fuelTank, int doors) {
        this.topSpeed = topSpeed;
        this.fuelTank = fuelTank;
        this.doors = doors;
    }

    public double getTopSpeed() {
        return topSpeed;
    }

    public double getFuelTank() {
        return fuelTank;
    }

    public int getDoors() {
        return doors;
    }

    public double range() {
        return 100 * fuelTank / LITRE_PER_100KM;
    }
}

// SportsCar.java
public class SportsCar extends Auto {
    double efficiency;

    public SportsCar(double topSpeed, double fuelTank, int doors) {
        super(topSpeed, fuelTank, doors);
        if (topSpeed > 200.0) {
            efficiency = 200.0 / topSpeed;
        } else {
            efficiency = 1.0;
        }
    }

    public double range() {
        return 100 * fuelTank * efficiency / LITRE_PER_100KM;
    }
}

// Main.java
public class Main2 {
    public static void main(String[] args) {
        Auto polo = new Auto();
        Auto focus = new Auto(190.5, 54.8, 5);

        SportsCar porsche911 = new SportsCar(298.9, 69.9, 2);
        SportsCar supra = new SportsCar(297.4, 58.9, 2);

        Auto[] garaza = new Auto[4];
        garaza[0] = polo;
        garaza[1] = focus;
        garaza[2] = porsche911;
        garaza[3] = supra;

        System.out.println("VW Polo: " + polo.range());
        System.out.println("Ford Focus: " + focus.range());

        System.out.println("911: " + porsche911.range());
        System.out.println("Toyota Supra: " + supra.range());

        System.out.println("\nGaraza 0: " + garaza[0].range());
        System.out.println("Garaza 1: " + garaza[1].range());
        System.out.println("Garaza 2: " + garaza[2].range());
        System.out.println("Garaza 3: " + garaza[3].range());
    }
}
