package Models.Database;

import Models.PensioneRepository;
import Models.Pensioner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class PensionerDatabase implements PensioneRepository {
    // baza:
    List<Pensioner> database = new ArrayList<Pensioner>();

    // logger:
    static Logger log = Logger.getLogger("Models/Database/PensionerDatabase.java");

    // konstruktor:
    public PensionerDatabase() {
        FileHandler fileHandler;

        try {
            fileHandler = new FileHandler("PensionerDatabase.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            log.info("Logger initialized!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        // nekoliko inserta u DB:
        database.add(new Pensioner(UUID.randomUUID(), "Drago", "Dragicevic", 75000, 55, 60));
        database.add(new Pensioner(UUID.randomUUID(), "Marija", "Marincic", 90000, 71, 58));
        database.add(new Pensioner(UUID.randomUUID(), "Ivan", "Ivanicevic", 82000, 45, 64));
    }

    @Override
    public Pensioner get(UUID id) {
        for (Pensioner p : database) {
            if (p.getUuid().compareTo(id) == 0) {
                return p;
            }
        }

        return null;
    }

    @Override
    public void add(Pensioner pensioner) {
        database.add(pensioner);
    }

    @Override
    public boolean update(Pensioner pensioner) {
        for (Pensioner p : database) {
            if (p.getUuid() == pensioner.getUuid()) {
                p.setFirstName(pensioner.getFirstName());
                p.setLastname(pensioner.getLastname());
                p.setAccountBalance(pensioner.getAccountBalance());
                p.setPensionInput(pensioner.getPensionInput());
                p.setWorkingAge(pensioner.getWorkingAge());

                return true;
            }
        }
        database.add(pensioner);
        return false;
    }

    @Override
    public void remove(Pensioner pensioner) {
        database.remove(pensioner);
    }

    @Override
    public List<Pensioner> getAllPensioners() {
        log.warning("GetAllPensioners called!");

        return database;
    }
}
