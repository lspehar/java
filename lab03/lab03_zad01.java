// Circle.java
public class Circle {
    public static final double PI = 3.14159;
    protected double r;

    public Circle(double r) {
        this.r = r;
    }

    public Circle() {
        this(3.2);
    }

    public double circumference() {
        return 2 * PI * r;
    }

    public double area() {
        return PI * r * r;
    }

    public double radius() {
        return r;
    }
}

// PlaneCircle.java
public class PlaneCircle extends Circle {
    protected double cx, cy;

    public PlaneCircle() {
    }

    public PlaneCircle(double r, double x, double y) {
        super(r);
        this.cx = x;
        this.cy = y;
    }

    public double getCentX() {
        return cx;
    }

    public double getCentY() {
        return cy;
    }

    public boolean isInside(double x, double y) {
        double dx = x - cx;
        double dy = y - cy;
        double distance = Math.sqrt(dx * dx + dy * dy);

        return (distance < r);
    }
}


// Main.java
public class Main {
    public static void main(String[] args) {
        //System.out.println("Test");

        PlaneCircle plane1 = new PlaneCircle();
        System.out.println("Radius: " + plane1.r);

        PlaneCircle plane2 = new PlaneCircle(3.5, 5, 2);
        double opseg = 2 * plane2.r * Circle.PI; // ne treba tu
        double povrsina = Math.pow(plane2.r, 2) * Circle.PI; // ne treba tu

        System.out.println("\nOpseg 1: " + plane2.circumference());
        System.out.println("Povrsina 1: " + plane2.area());

        System.out.println("Opseg 2: " + opseg); // ne treba tu
        System.out.println("Povrsina 2: " + povrsina); // ne treba tu
    }
}
