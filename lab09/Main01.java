// Printer.java
public class Printer<T> {
    T thingToPrint;

    Printer(T thingToPrint) {
        this.thingToPrint = thingToPrint;
    }

    T getThingToPrint() {
        return thingToPrint;
    }

    public void print() {
        System.out.println("Printer printing: " + getThingToPrint());
    }
}



// DoublePrinter.java
public class DoublePrinter {
    public double thingToPrint;

    public DoublePrinter(double thingToPrint) {
        this.thingToPrint = thingToPrint;
    }

    public double getThingToPrint() {
        return thingToPrint;
    }

    public void print() {
        System.out.println("Printing: " + getThingToPrint());
    }
}



// StringPrinter.java
public class StringPrinter {
    public String thingToPrint;

    public StringPrinter(String thingToPrint) {
        this.thingToPrint = thingToPrint;
    }

    public String getThingToPrint() {
        return thingToPrint;
    }

    public void print() {
        System.out.println("Printing: " + getThingToPrint());
    }
}



// IntegerPrinter.java
public class IntegerPrinter {
    public int thingToPrint;

    public IntegerPrinter(int thingToPrint) {
        this.thingToPrint = thingToPrint;
    }

    public int getThingToPrint() {
        return thingToPrint;
    }

    public void print() {
        System.out.println("Printing: " + getThingToPrint());
    }
}



// Main.java
public class Main {
    public static void main(String[] args) {
        // Printeri po klasama:
        DoublePrinter printer1 = new DoublePrinter(7.6);
        StringPrinter printer2 = new StringPrinter("title");
        IntegerPrinter printer3 = new IntegerPrinter(76);

        printer1.print();
        printer2.print();
        printer3.print();

        // 3 Printera PRINTER:
        Printer<Integer> P1 = new Printer<Integer>(64);
        Printer<String> P2 = new Printer<String>("T type print");
        Printer<Double> P3 = new Printer<Double>(9.76);

        System.out.println("\nPrinters:");
        P1.print();
        P2.print();
        P3.print();
    }
}
