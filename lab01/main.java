import java.util.Locale;
import java.util.Scanner;
import java.lang.String;

/*
class main {
    public static void main(String[] args) {
        System.out.println("Unesi n: ");
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        System.out.println("N je " + n);

        for (int i = 0; i <= n; i++) {
            if (i % 2 == 0) {
                System.out.println(i + "\t");
            }
        }

    }
}

class main {
    public static void main(String[] args) {
        System.out.println("Unesi string: ");
        Scanner input = new Scanner(System.in);
        String str = input.nextLine();

        System.out.println(wordCount(str));

    }

    public static int wordCount(String str) {
        int counter = 0;
        if (!(" ".equals(str.substring(0, 1))) || !(" ".equals(str.substring(str.length() - 1)))) {
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == ' ') {
                    counter++;
                }
            }
            counter = counter + 1;
        }

        return counter;
    }

}

class main {
    public static void main(String[] args) {
        System.out.println("Unesi string: ");
        Scanner input = new Scanner(System.in);
        String str_def = input.nextLine();
        String str = str_def.toLowerCase();

        if (palindrom(str)) {
            System.out.println("String je palindrom!");
        } else {
            System.out.println("String nije palindrom!");
        }

    }

    static boolean palindrom(String str) {
        int i = 0;
        int j = str.length() - 1;

        while (i < j) {
            if (str.charAt(i) != str.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}
*/
class main {
    public static void main(String[] args) {
        System.out.println("Uneis broj: ");
        Scanner in = new Scanner(System.in);

        int num = in.nextInt();
        System.out.println("Broj je " + num + "\n");

        int sto = (num / 100) % 100;
        int des = (num / 10) % 10;
        int jed = num % 10;

        System.out.println("STO: " + sto);
        System.out.println("DES: " + des);
        System.out.println("JED: " + jed);

        // provjera broja:
        /*
        if (num < 100 || num > 999) {
            System.out.println("Broj nevalja!");
        } else {
            System.out.println("Broj je " + num);
        } */



    }
}