// Animal.java
public class Animal {
    public String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}



// Cat.java
public class Cat extends Animal {
    public void glasanje() {
        System.out.println("Meow!");
    }
}



// Dog.java
public class Dog extends Animal {
    public void glasanje() {
        System.out.println("Whoof!");
    }
}



// Main.java
import java.util.ArrayList;

public class Main2 {
    public static void main(String[] args) {
        ArrayList<Cat> cats = new ArrayList<>();

        cats.add(new Cat());
        cats.add(new Cat());
        cats.add(new Dog()); // wrong - greska!

        // castanje (nije potrebno ako je arraylist tipa Cat!):
        Cat myCat1 = (Cat) cats.get(0);
        Cat myCat2 = (Cat) cats.get(1);
        Cat myCat3 = (Cat) cats.get(2); // crna ovca
    }
}
