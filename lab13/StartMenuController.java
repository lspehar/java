package com.example.loginprozor;

import Models.Database.PensionerDatabase;
import Models.Database.StudentDatabase;
import Models.Database.WorkerDatabase;
import Models.Pensioner;
import Models.Student;
import Models.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.util.UUID;

public class StartMenuController {

    @FXML
    // dodati deklaraciju Label userLabel varijable s druge scene
    Label userLabel;

    // napisati setUser metodu koja kao argument prima String, a postavlja tekst userLabel-a na
    public void generateSampleData(ActionEvent event){
        Student student = new Student(UUID.randomUUID(),"Mato", "Perić",3000.0,40);
        Pensioner pensioner = new Pensioner(UUID.randomUUID(),"Ivan", "Danić",4000.00f, 3000.0,40);
        Worker worker = new Worker(UUID.randomUUID(),"Zlatibor", "Pelinkovac",5000.00f,4,8560,true);

        userLabel.setText(student.toString() + "\n"+pensioner.toString()+"\n"+ worker.toString()+"\n" );

        System.out.println("Student tax : "+student.getCalculatedTax());
        System.out.println("Pension tax :"+pensioner.getCalculatedTax());
        System.out.println("Worker tax ."+worker.getCalculatedTax());

        StudentDatabase sdb = new StudentDatabase();
        sdb.getAllStudents().stream().forEach(student1 -> System.out.println(student1.toString()));
        System.out.println("ovo je jedan dobar red");

        WorkerDatabase wdb = new WorkerDatabase();
        wdb.getAllWorkers().stream().forEach(worker1 -> System.out.println(worker1.toString()));
        System.out.println("Worker - odabran red");

        PensionerDatabase pdb = new PensionerDatabase();
        pdb.getAllPensioners().stream().forEach(pensioner1 -> System.out.println(pensioner1.toString()));
        System.out.println("Pensioner - odabran red");


    }

}
