import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.Buffer;
import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {
        System.out.println("Unesi info: ");

        // Scanner za input:
        Scanner input = new Scanner(System.in);
        String in1 = input.nextLine();
        String in2 = input.nextLine();
        String in3 = input.nextLine();
        String in4 = input.nextLine();
        String in5 = input.nextLine();

        // System.out.println(in1 + "\n" + in2);

        // writer - upisuje u text file:
        FileWriter fileWriter;

        try {
            fileWriter = new FileWriter("dosje.txt");
            BufferedWriter buffWrite = new BufferedWriter(fileWriter);
            buffWrite.write(in1 + "\n");
            buffWrite.write(in2 + "\n");
            buffWrite.write(in3 + "\n");
            buffWrite.write(in4 + "\n");
            buffWrite.write(in5 + "\n");

            buffWrite.close();
            System.out.println("Upisao u file!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

