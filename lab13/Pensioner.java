package Models;

import Utilities.GeneralTaxingConstants;

import java.util.Objects;
import java.util.UUID;

public class Pensioner extends Person{
    protected  double pensionInput;
    protected int workingAge;

    private final double fullWorkingAge = 1;
    private final double semiWorkingAge = 0.2;
    private final double lowWorkingAge = 0.4;

    private final int pensionThreshold = 2000;
    private final int fullWorkingThresholdInYears = 40;
    private final int semiWorkingThresholdInYears = 30;

    public Pensioner(UUID uuid,String firstName, String lastName, float accountBalance, double pensionInput, int workingAge){
super(uuid,firstName,lastName,accountBalance);
this.pensionInput=pensionInput;
this.workingAge=workingAge;
    }

    public Pensioner(Pensioner original){
        super(UUID.randomUUID(),original.firstName,original.lastname,original.accountBalance);
        this.pensionInput=original.pensionInput;
        this.workingAge = original.workingAge;
    }

    public double getPensionInput() {
        return pensionInput;
    }

    public int getWorkingAge() {
        return workingAge;
    }

    public void setPensionInput(double pensionInput) {
        this.pensionInput = pensionInput;
    }

    public void setWorkingAge(int workingAge) {
        this.workingAge = workingAge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pensioner pensioner = (Pensioner) o;
        return Double.compare(pensioner.pensionInput, pensionInput) == 0 &&
                workingAge == pensioner.workingAge &&
                Double.compare(pensioner.fullWorkingAge, fullWorkingAge) == 0 &&
                Double.compare(pensioner.semiWorkingAge, semiWorkingAge) == 0 &&
                Double.compare(pensioner.lowWorkingAge, lowWorkingAge) == 0 &&
                pensionThreshold == pensioner.pensionThreshold;

    }

    @Override
    public int hashCode() {
        return Objects.hash(pensionInput, workingAge, fullWorkingAge, semiWorkingAge, pensionThreshold );
    }


    @Override
    public String toString() {
        return "Pensioner{" +
                "pensionInput=" + pensionInput +
                ", workingAge=" + workingAge +
                ", fullWorkingAge=" + fullWorkingAge +
                ", semiWorkingAge=" + semiWorkingAge +
                ", lowWorkingAge=" + lowWorkingAge +
                ", pensionThreshold=" + pensionThreshold +
                ", fullWorkingThresholdInYears=" + fullWorkingThresholdInYears +
                ", semiWorkingThresholdInYears=" + semiWorkingThresholdInYears +
                ", uuid=" + uuid +
                ", firstName='" + firstName + '\'' +
                ", lastname='" + lastname + '\'' +
                ", accountBalance=" + accountBalance +
                '}';
    }

    @Override
    public boolean applyTax() {
        double calculatedTax = 0;
        if (workingAge > fullWorkingThresholdInYears && pensionInput > pensionThreshold) {
            calculatedTax = accountBalance * GeneralTaxingConstants.TAX_PENSIONER * fullWorkingAge;
            accountBalance = accountBalance - calculatedTax;
            return true;
        } else if ((workingAge > semiWorkingThresholdInYears && workingAge < fullWorkingThresholdInYears) && pensionInput > pensionThreshold) {
            calculatedTax = accountBalance * GeneralTaxingConstants.TAX_PENSIONER * semiWorkingAge;
            accountBalance = accountBalance - calculatedTax;
            return true;
        }else if ((workingAge < 25 && pensionInput>pensionThreshold)){
            calculatedTax = accountBalance*GeneralTaxingConstants.TAX_PENSIONER*lowWorkingAge;
            accountBalance=accountBalance-calculatedTax;
            return true;
        }else {
            return false;
        }

    }

    @Override
    public double getCalculatedTax() {
        double calculatedTax;
        if (workingAge >= fullWorkingThresholdInYears && pensionInput > pensionThreshold) {
            calculatedTax = accountBalance * GeneralTaxingConstants.TAX_PENSIONER * fullWorkingAge;

            return calculatedTax;
        } else if ((workingAge > semiWorkingThresholdInYears && workingAge < fullWorkingThresholdInYears) && pensionInput > pensionThreshold) {
            calculatedTax = accountBalance * GeneralTaxingConstants.TAX_PENSIONER * semiWorkingAge;

            return calculatedTax;
        } else if ((workingAge < 25 && pensionInput > pensionThreshold)) {
            calculatedTax = accountBalance * GeneralTaxingConstants.TAX_PENSIONER * lowWorkingAge;

            return calculatedTax;
        } else {
            return calculatedTax = 0;
        }
    }
}

