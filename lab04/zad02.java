// Circle.java
public class Circle {
    public static final double PI = 3.14159;
    protected double r;

    public Circle(double r) {
        checkRadius(r);
        this.r = r;
    }

    public double getRadius() {
        return r;
    }

    public void setRadius(double r) {
        checkRadius(r);
        this.r = r;
    }

    protected void checkRadius(double radius) {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius je negativan!");
        }
    }

    public double area() {
        return PI * r * r;
    }

    public double circumference() {
        return 2 * PI * r;
    }
}

// Main.java
public class Main {
    public static void main(String[] args) {
        Circle c1 = new Circle(5.56);
        Circle c2 = new Circle(7.62);
        Circle c3 = new Circle(3.08);

        System.out.println("Round 1:");
        System.out.println("C1: " + c1.area() + ", " + c1.circumference());
        System.out.println("C2: " + c2.area() + ", " + c2.circumference());
        System.out.println("C3: " + c3.area() + ", " + c3.circumference());

        System.out.println("\nRound 2:");
        c1.setRadius(56.6);
        System.out.println("C1: " + c1.area() + ", " + c1.circumference());
        c2.setRadius(6.27);
        System.out.println("C2: " + c2.area() + ", " + c2.circumference());
        c3.setRadius(8.03);
        System.out.println("C3: " + c3.area() + ", " + c3.circumference());
    }
}
