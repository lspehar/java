package com.example.lab12;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginView {
    @FXML
    private TextField username;
    @FXML
    private Button btnLogin;
    @FXML
    private PasswordField password;
    @FXML
    private Label infoLabel;

    private Parent root;
    private Stage stage;
    private Scene scene;

    @FXML
    void onLoginClicked(ActionEvent event) throws IOException {
        // Spremanje teksta iz TextFielda username u varijablu
        String user = username.getText();

        // Provjera username-a i password-a
        if (username.getText().equalsIgnoreCase("luka") && password.getText().equalsIgnoreCase("luka")) {
            // Dohvaćanje scene
            FXMLLoader loader = new FXMLLoader(getClass().getResource("inside-view.fxml"));
            // loadanje scene u root varijablu
            //dohvaćanje stagea iz glavne klase
            root = loader.load();
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            //postavljanje root node-a na novu scenu
            scene = new Scene(root,600,500);
            // postavljanje nove scene na stage
            stage.setScene(scene);
            stage.show();

            InsideView startMenuController = loader.getController();
            startMenuController.setUser(user);

        } else {
            infoLabel.setText("Neispravni username i/ili password!");
        }
    }
}
