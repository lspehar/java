package Models;

import java.util.List;
import java.util.UUID;

public interface WorkerRepository {
    // crud metode
    public Worker get(UUID id);
    public void add(Worker worker);
    public boolean update(Worker worker);
    public void remove(Worker worker);

    // lista:
    public List<Worker> getAllWorkers();
}

