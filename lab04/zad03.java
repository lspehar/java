// Shape.java
public abstract class Shape {
    public abstract double area();
    public abstract double circumference();
}

// Circle.java
public class Circle extends Shape {
    public static final double PI = 3.14159;
    protected double r;

    Circle2(double r) {
        this.r = r;
    }

    public double getRadius() {
        return r;
    }

    public double area() {
        return PI * r * r;
    }

    public double circumference() {
        return 2 * PI * r;
    }
}

// Rectangle.java
public class Rectangle extends Shape {
    protected double w;
    protected double h;

    Rectangle(double w, double h) {
        this.w = w;
        this.h = h;
    }

    public double getWidth() {
        return w;
    }

    public double getHeight() {
        return h;
    }

    public double area() {
        return w * h;
    }

    public double circumference() {
        return 2 * (h + w);
    }
}

// Main.java
public class Main3 {
    public static void main(String[] args) {
        // tbc
    }
}
