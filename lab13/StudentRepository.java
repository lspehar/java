package Models;

import java.util.List;
import java.util.UUID;

public interface StudentRepository {
    // crud metode
    public Student get(UUID id);
    public void add(Student student);
    public boolean update(Student student);
    public void remove(Student student);

    // lista:
    public List<Student> getAllStudents();
}

