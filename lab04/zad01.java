// A.java
package paket;

public class A {
    protected final String name;

    public A(String name) {
        this.name = name;
    }

    public String GetName() {
        return name;
    }
}

// B.java
package paket;

public class B extends A {
    public B(String name) {
        super(name);
    }

    public String GetName() {
        return name;
    }

    public String Examine(A ime) {
        return ime.GetName();
    }
}

// C.java
public class C extends paket.A {
    public C(String name) {
        super(name);
    }

    public String GetName() {
        return name;
    }

    public String Examine(paket.A ime) {
        return ime.GetName();
    }
}

// Main.java
import paket.*;

public class Main {
    public static void main(String[] args) {
        A prvi = new A("Ich bin A");
        B drugi = new B("I am B");
        C treci = new C("Ja C");

        System.out.println(prvi.GetName());

        System.out.println(drugi.GetName());
        System.out.println("Examine B: " + drugi.Examine(prvi));

        System.out.println(treci.GetName());
        System.out.println("Examine C: " + treci.Examine(prvi));
    }
}
