package Models;

import java.util.List;
import java.util.UUID;

public interface PensioneRepository {
    // crud metode
    public Pensioner get(UUID id);
    public void add(Pensioner pensioner);
    public boolean update(Pensioner pensioner);
    public void remove(Pensioner pensioner);

    // lista:
    public List<Pensioner> getAllPensioners();

}
